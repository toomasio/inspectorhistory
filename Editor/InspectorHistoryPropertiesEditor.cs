﻿using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Digitom.InspectorHistory.Editor
{
    public class InspectorHistoryPropertiesEditor : EditorWindow
    {
        [SerializeField] private VisualTreeAsset visualAsset;
        private InspectorHistoryData data;

        [MenuItem("Window/Inspector History/Preferences")]
        public static void ShowWindow()
        {
            GetWindow<InspectorHistoryPropertiesEditor>("History Preferences");
        }

        private void OnEnable()
        {
            if (!data)
            {
                data = InspectorHistoryEditor.GetData();
            }
        }

        private void CreateGUI()
        {
            var root = rootVisualElement;
            var element = visualAsset.CloneTree();
            root.Add(element);

            var ser = new SerializedObject(data);
            var contentProp = ser.FindProperty("content");
            var enableFavouritesProp = contentProp.FindPropertyRelative("enableFavourites");
            var maxFavouritesProp = contentProp.FindPropertyRelative("maxFavouritesPerRow");

            var enableFavouritesElement = element.Q<Toggle>("toggle-field__enable-favourites");
            var maxFavouritesElement = element.Q<IntegerField>("int-field__max-favourites");

            enableFavouritesElement.BindProperty(enableFavouritesProp);
            maxFavouritesElement.BindProperty(maxFavouritesProp);
        }
    }
}


