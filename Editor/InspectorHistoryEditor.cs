﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Digitom.InspectorHistory.Editor
{
    [InitializeOnLoad]
    public class InspectorHistoryEditor : EditorWindow
    {
        [SerializeField] private VisualTreeAsset rootAsset;
        [SerializeField] private VisualTreeAsset rowFavouritesAsset;
        [SerializeField] private VisualTreeAsset buttonFavouritesAsset;
        private TemplateContainer rootWindow;
        private IntegerField intFieldCache;
        private Button buttonClear;
        private Button buttonPrevious;
        private Button buttonNext;
        private Button buttonFavourite;
        private VisualElement containerFavourites;
        private InspectorHistoryData data;
        private InspectorHistoryData.InspectorHistoryContainer content;
        private int curRows;
        private int curSelection;

        [MenuItem("Window/Inspector History/History")]
        public static void ShowWindow()
        {
            GetWindow<InspectorHistoryEditor>("History");
        }

        private void OnEnable()
        {
            if (!data)
                data = GetData();
            data.InitializeData();
            content = data.content;
        }

        private void OnDisable()
        {
            ClearCache();
        }

        private void OnDestroy()
        {
            ClearCache();
        }

        public static InspectorHistoryData GetData()
        {
            var dir = InspectorHistoryUtils.DIRPATH;
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            var path = $"{dir}/InspectorHistoryData.asset";

            //get data
            var data = AssetDatabase.LoadAssetAtPath<InspectorHistoryData>(path);
            if (data)
                return data;
            else//create if doesnt exist
            {
                var asset = CreateInstance<InspectorHistoryData>();
                AssetDatabase.CreateAsset(asset, path);
                AssetDatabase.SaveAssets();
                data = asset;
            }
            return data;
        }

        private void CreateGUI()
        {
            CalculateWindowHeight();

            DrawButtons();
            DrawFavourites();
        }

        private void DrawButtons()
        {
            var root = rootVisualElement;
            root.Clear();
            rootWindow = rootAsset.CloneTree();
            root.Add(rootWindow);

            //query
            intFieldCache = rootWindow.Q<IntegerField>("int-field-cache");
            buttonClear = rootWindow.Q<Button>("button-clear");
            buttonPrevious = rootWindow.Q<Button>("button-previous");
            buttonNext = rootWindow.Q<Button>("button-next");
            buttonFavourite = rootWindow.Q<Button>("button-favourite");

            intFieldCache.RegisterValueChangedCallback((e) => { content.cache = e.newValue; });

            buttonClear.clicked += () => ClearCache();

            string prevButton = "◄";
            if (content.prevInd >= content.prevObjects.Count - 1 && !(content.prevObjects.Count > 0 && content.prevInd == 0))
                prevButton = "◄✖";
            buttonPrevious.text = prevButton;
            buttonPrevious.clicked += () => Previous();

            string nextButton = "►";
            if (content.nextInd >= content.nextObjects.Count - 1 && !(content.nextObjects.Count > 0 && content.nextInd == 0))
                nextButton = "✖►";
            buttonNext.text = nextButton;
            buttonNext.clicked += () => Next();

            buttonFavourite.clicked += () =>
            {
                if (Selection.assetGUIDs.Length > 0)
                    AddFavourite(Selection.assetGUIDs[0]);
                else
                    Debug.Log("Can only favourite project objects at this time due to resets. Sorry!");
            };

        }

        private void DrawFavourites()
        {
            //favorites
            containerFavourites = rootWindow.Q<VisualElement>("container-favourites");
            containerFavourites.Clear();
            if (content.enableFavourites && content.favourites.Count > 0)
            {
                //calculate how many row we need based on count and per row
                curRows = CalculateRows(content.favourites.Count, content.maxFavouritesPerRow);

                //populate the rows
                int lastIndex = 0;
                for (int i = 0; i < curRows; i++)
                {
                    var rowElementAsset = rowFavouritesAsset.CloneTree();
                    var rowElement = rowElementAsset.Q<VisualElement>("row-favourites");
                    containerFavourites.Add(rowElement);

                    int rowElementCount = 0;
                    for (int j = lastIndex; j < content.favourites.Count; j++)
                    {
                        if (rowElementCount < content.maxFavouritesPerRow)
                        {
                            var obj = GetObject(content.favourites[j]);
                            var buttonElementAsset = buttonFavouritesAsset.CloneTree();
                            
                            var buttonElement = buttonElementAsset.Q<VisualElement>("container__button-clearable");
                            var clearable = new ButtonClearable 
                            {
                                element = buttonElement,
                                index = j
                            };
                            rowElement.Add(buttonElement);

                            var buttonMain = buttonElement.Q<Button>("button-main");
                            var buttonClear = buttonElement.Q<Button>("button-clear");

                            buttonMain.text = obj.name;
                            buttonMain.clicked += () => Selection.activeObject = obj;
                            buttonClear.clicked += () => RemoveFavourite(clearable);
                            rowElementCount++;
                        }
                        else
                        {
                            lastIndex = j;
                            break;
                        }

                    }
                }
            }
            else
                curRows = 0;
        }

        int CalculateRows(int totalItems, int maxItemsPerRow)
        {
            if (totalItems <= 0 || maxItemsPerRow <= 0)
            {
                // Handle invalid input
                return 0;
            }

            // Calculate the number of rows needed
            int rows = totalItems / maxItemsPerRow;
            if (totalItems % maxItemsPerRow != 0)
            {
                rows++; // Increment if there are remaining items
            }

            return rows;
        }

        void CalculateWindowHeight()
        {
            content.height = 24;
            if (content.enableFavourites)
            {
                for (int i = 0; i < curRows; i++)
                {
                    content.height += 24;
                }
            }
            minSize = new Vector2(minSize.x, content.height);
        }

        Object GetObject(int _id)
        {
            return EditorUtility.InstanceIDToObject(_id);
        }

        Object GetObject(string _guid)
        {
            string path = AssetDatabase.GUIDToAssetPath(_guid);
            if (string.IsNullOrEmpty(path))
                return null;
            var obj = AssetDatabase.LoadAssetAtPath<Object>(path);
            if (obj != null)
                return obj;

            return null;
        }

        void Previous()
        {
            if (content.prevObjects.Count < 1)
            {
                content.prevInd = 0;
                return;
            }

            if (!content.goingBack)
            {
                content.goingBack = true;
                content.goingForward = false;
                content.prevInd = 0;
            }
            else
                content.prevInd++;

            //only add to list if searching isnt maxed out
            if (content.prevInd > content.prevObjects.Count - 1)
                content.prevInd = content.prevObjects.Count - 1;
            else
            {
                //add to next object list

                AddToList(content.nextObjects, content.curObject, true);
                content.nextInd = 0;

                //set current object
                content.curObject = content.prevObjects[content.prevInd];
                Selection.activeObject = GetObject(content.curObject);
            }

            CreateGUI();
        }

        void Next()
        {
            if (content.nextObjects.Count < 1)
            {
                content.nextInd = 0;
                return;
            }

            if (!content.goingForward)
            {
                content.goingForward = true;
                content.goingBack = false;
                content.nextInd = 0;
            }
            else
                content.nextInd++;

            //only add to list if searching isnt maxed out
            if (content.nextInd > content.nextObjects.Count - 1)
                content.nextInd = content.nextObjects.Count - 1;
            else
            {
                //add to previous list
                AddToList(content.prevObjects, content.curObject, true);
                content.prevInd = 0;
                content.curObject = content.nextObjects[content.nextInd];
                Selection.activeObject = GetObject(content.curObject);
            }

            CreateGUI();
        }

        private void OnSelectionChange()
        {
            if (Selection.activeObject)
                curSelection = Selection.activeInstanceID;
            else
                return;

            if (content == null) return;

            if (content.goingBack)
            {
                if (content.prevObjects.Count > 0)
                {
                    if (curSelection == content.prevObjects[content.prevInd])
                        return;
                    else
                    {
                        content.prevInd = 0;
                        content.nextObjects.Clear();
                        content.goingBack = false;
                    }
                }

            }
            if (content.goingForward)
            {
                if (content.nextObjects.Count > 0)
                {
                    if (Selection.activeObject == GetObject(content.nextObjects[content.nextInd]))
                        return;
                    else
                    {
                        content.nextInd = 0;
                        content.nextObjects.Clear();
                        content.goingForward = false;
                    }
                }

            }

            AddToList(content.prevObjects, content.curObject, true);
            content.curObject = curSelection;

            CreateGUI();
        }

        void RemoveFavourite(ButtonClearable button)
        {
            RemoveFavourite(button.index);
        }

        void RemoveFavourite(int index)
        {
            content.favourites.RemoveAt(index);
            data.WriteDataToFile();

            CreateGUI();
        }

        void AddFavourite(string _guid)
        {
            if (Selection.activeObject)
            {
                if (!content.favourites.Contains(_guid))
                {
                    content.favourites.Add(_guid);
                }
            }
            data.WriteDataToFile();
            CalculateWindowHeight();

            CreateGUI();
        }

        void AddToList(List<int> _list, int _object, bool _onlyAddNew = false)
        {
            if (_onlyAddNew && _list.Contains(_object))
                return;

            if (_list.Count < 1)
                _list.Add(_object);
            else
                _list.Insert(0, _object);

            if (_list.Count > content.cache)
                _list.RemoveRange(content.cache - 1, _list.Count - content.cache);
        }

        void ClearCache()
        {
            content.prevObjects.Clear();
            content.nextObjects.Clear();
            content.prevInd = 0;
            content.nextInd = 0;
            content.goingBack = false;
            content.goingForward = false;

            data.WriteDataToFile();

            CreateGUI();
        }

    }
}


