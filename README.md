Install Instructions:
----------------------
Unity -> Window -> Package Manager -> "+" -> Add package from git URL ... -> https://gitlab.com/toomasio/inspectorhistory.git
After Unity loads the package, Window -> Inspector History

Data Directory = "Assets/Plugins/Digitom/InspectorHistory"

Hope this saves people some time clicking around!