﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Digitom.InspectorHistory
{
    public class InspectorHistoryData : ScriptableObject
    {
        [System.Serializable]
        public class InspectorHistoryContainer
        {
            public int curObject;
            public List<int> prevObjects = new List<int>();
            public List<int> nextObjects = new List<int>();
            public bool goingBack;
            public bool goingForward;
            public int prevInd;
            public int nextInd;
            public int cache = 5;
            public float height;
            public bool enableFavourites = true;
            public int maxFavouritesPerRow = 4;
            public List<string> favourites = new List<string>();
        }

        public InspectorHistoryContainer content = new InspectorHistoryContainer();
        private string dataPath;

        void OnEnable()
        {
            dataPath = $"{InspectorHistoryUtils.DIRPATH}/InspectorHistorySave.json";
        }

        public void InitializeData()
        {
            GetDataFromFile();
        }

        void GetDataFromFile()
        {
            if (!File.Exists(dataPath))
                WriteDataToFile();
            else
                ReadDataFromFile();
        }

        public void WriteDataToFile()
        {
            var json = JsonUtility.ToJson(content);
            File.WriteAllText(dataPath, json);
        }

        void ReadDataFromFile()
        {
            var data = File.ReadAllText(dataPath);
            content = JsonUtility.FromJson<InspectorHistoryContainer>(data);
        }

    }
}


